export class StatisticsModel {
  orders: number = 0;
  revenue: number = 0;
  customers: number = 0;
  graph: any;
}
