import { Component, OnInit } from '@angular/core';
import {StatisticsService} from './statistics.service';
import {ResponseModel} from '../models/response.model';
import {StatisticsModel} from '../models/statistics.model';
import * as moment from 'moment';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  data: StatisticsModel;
  errorMessage: string;
  showGraph = false;
  from: any;
  until: any;
  updateGraphValue: number = 0;

  constructor(private statistics: StatisticsService) {
    this.data = new StatisticsModel();
    this.setDefaultDates();
  }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    const from = moment(this.from, 'YYYY-MM-DD').format('DD-MM-YYYY');
    const until = moment(this.until, 'YYYY-MM-DD').format('DD-MM-YYYY');

    this.statistics.getStatistics(from, until).subscribe((response: ResponseModel) => {
      if (response.success === false) {
        console.error('Something goes wrong with getStatistics');
        return false;
      }

      this.data = response.data;
      this.loadGraph();
    });
  }

  private loadGraph() {
    this.showGraph = true;
  }

  submit() {
    this.errorMessage = '';
    const from = moment(this.from, 'YYYY-MM-DD');
    const until = moment(this.until, 'YYYY-MM-DD');

    if (from.isValid() === false || until.isValid() === false) {
      this.errorMessage = 'Please enter valid dates';
      return false;
    } else if (from > until) {
      this.errorMessage = 'From date is greater than until';
      return false;
    }

    this.statistics.getStatistics(from.format('DD-MM-YYYY'), until.format('DD-MM-YYYY')).subscribe((response: ResponseModel) => {
      if (response.success === false) {
        console.error('Something goes wrong with getStatistics');
        return false;
      }

      this.data = response.data;
      this.loadGraph();
      this.updateGraph();
    });
  }

  private setDefaultDates() {
    this.from = moment().subtract('months', 1).format('YYYY-MM-DD');
    this.until = moment().format('YYYY-MM-DD');
  }

  private updateGraph() {
    this.updateGraphValue = Math.random() * 100;
  }
}
