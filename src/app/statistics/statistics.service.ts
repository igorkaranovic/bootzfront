import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class StatisticsService {
  constructor(private http: HttpClient) {}

  getStatistics(from: string, until: string) {
    const url = environment.apiUrl + '/orders/' + from + '/' + until;
    return this.http.get(url);
  }

}
