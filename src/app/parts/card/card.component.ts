import {Component, Input} from '@angular/core';

@Component({
  selector: 'info-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() title = '';
  @Input() text = '';
  @Input() value = '';
  @Input() icon;
}
