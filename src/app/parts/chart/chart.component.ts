import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import * as Highcharts from 'highcharts';
import {Chart} from 'highcharts';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @Input() data: any;
  public options: any;
  private chart: Chart;

  @Input('update')
  set update(value: number) {
    if (value === 0) {
      return;
    }

    this.updateGraph();
  }

  constructor() {
    this.getOptions();
  }

  ngOnInit() {
    this.prepareData();
  }

  public prepareData() {
    if (typeof this.data === 'undefined') {
      return false;
    }

    const orders = this.getOrdersData();
    const customers = this.getCustomersData();

    this.options.series = [
      {
        name: 'Orders',
        data: orders
      },
      {
        name: 'Customers',
        data: customers
      }];

    this.loadGraph();
  }

  private loadGraph() {
    this.chart = Highcharts.chart('container', this.options);
  }

  private updateGraph() {
    this.prepareData();
  }

  private getOptions() {
    this.options = {
      chart: {
        type: 'line'
      },
      title: {
        text: ''
      },
      credits: {
        enabled: false
      },
      xAxis: {
        type: 'datetime',
        labels: {
          formatter() {
            return Highcharts.dateFormat('%b %e', this.value);
          }
        }
      },
      yAxis: {
        title: {
          text: ''
        }
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          }
        }
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      series: [],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 1000
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'top'
            }
          }
        }]
      }
    };
  }

  private getOrdersData() {
    const data = [];
    this.data.orders.forEach((order) => {
      data.push([
        Date.parse(order.date),
        order.count
      ]);
    });

    return data;
  }

  private getCustomersData() {
    const data = [];
    this.data.customers.forEach((order) => {
      data.push([
        Date.parse(order.date),
        order.count
      ]);
    });

    return data;
  }

}
